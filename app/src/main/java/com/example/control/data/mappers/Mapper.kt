package com.example.control.data.mappers

import com.example.control.data.room.entities.Outgoing
import com.example.control.data.vo.OutgoingVo
import com.example.control.presenter.utils.Type

object Mapper {
    fun fromVoToEntity(outgoingVo: OutgoingVo) = Outgoing(outgoingVo.id,outgoingVo.cost,outgoingVo.info,outgoingVo.date,outgoingVo.type)
    fun fromEntityToVo(outgoing: Outgoing) = OutgoingVo(outgoing.id,outgoing.cost,outgoing.info,outgoing.date,outgoing.type)
}