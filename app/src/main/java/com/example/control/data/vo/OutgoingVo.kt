package com.example.control.data.vo

data class OutgoingVo(var id: Long = 0, var cost: Long = -1, var info: String = "" , var date: String = "", var type: Int = -1)