package com.example.control.data.room.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Outgoing(
    @PrimaryKey(autoGenerate = true)
    var id:Long,
    var cost:Long,
    var info:String,
    var date:String,
    var type:Int
)