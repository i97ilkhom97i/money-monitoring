package com.example.control.data.room.daos

import androidx.room.Dao
import androidx.room.Query
import com.example.control.data.room.entities.Outgoing

@Dao
interface OutgoingDao : BaseDao<Outgoing> {

    @Query("SELECT * FROM Outgoing")
    fun getOutgoings() : List<Outgoing>

    @Query("SELECT * FROM Outgoing WHERE id = :id")
    fun isHasOutgoing(id:Long) : Boolean

    @Query("SELECT * FROM Outgoing WHERE date LIKE :month")
    fun getByMonth(month:String) : List<Outgoing>
}