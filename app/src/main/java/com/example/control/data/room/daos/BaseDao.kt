package com.example.control.data.room.daos

import androidx.room.*

@Dao
interface BaseDao<T> {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(data:T) : Long

    @Update
    fun update(data: T) : Int

    @Delete
    fun delete(data: T) : Int
}