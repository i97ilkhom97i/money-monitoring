package com.example.control.data.repositories

import com.example.control.data.mappers.Mapper
import com.example.control.data.room.daos.OutgoingDao
import com.example.control.data.vo.OutgoingVo
import com.example.control.domain.repositories.OutgoingRepository
import javax.inject.Inject

class OutgoingRepositoryImpl @Inject constructor(private val dao: OutgoingDao) : OutgoingRepository {


    override fun insert(data: OutgoingVo): Long = dao.insert(Mapper.fromVoToEntity(data))

    override fun update(data: OutgoingVo): Int = dao.update(Mapper.fromVoToEntity(data))

    override fun delete(data: OutgoingVo): Int = dao.delete(Mapper.fromVoToEntity(data))

    override fun getAll(): List<OutgoingVo> = dao.getOutgoings().map { Mapper.fromEntityToVo(it) }

    override fun getByMonth(month:String): List<OutgoingVo> = dao.getByMonth(month).map { Mapper.fromEntityToVo(it) }

    override fun isHasData(data: OutgoingVo): Boolean = dao.isHasOutgoing(data.id)
}