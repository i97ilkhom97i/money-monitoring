package com.example.control.data.room.databases

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.control.data.room.daos.OutgoingDao
import com.example.control.data.room.entities.Outgoing

@Database(entities = [Outgoing::class],version = 1)
abstract class AppDatabase : RoomDatabase(){
    abstract fun getOutgoingDao() : OutgoingDao
    companion object{
        @Volatile
        var INSTANCE : AppDatabase ?= null
        fun getDatabase(context: Context) : AppDatabase{
            val tempInstance = INSTANCE
            if (tempInstance != null){
                return tempInstance
            }
            synchronized(this){
                val instance = Room.databaseBuilder(context,AppDatabase::class.java,"database").allowMainThreadQueries().build()
                INSTANCE = instance
                return instance
            }
        }
    }
}