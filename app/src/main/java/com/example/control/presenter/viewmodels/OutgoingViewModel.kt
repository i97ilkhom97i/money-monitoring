package com.example.control.presenter.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.control.data.vo.OutgoingVo
import com.example.control.domain.usecases.OutgoingUseCase
import com.example.control.presenter.utils.Messages
import javax.inject.Inject

class OutgoingViewModel @Inject constructor(private val outgoingUseCase: OutgoingUseCase) : ViewModel() {
    private var _liveDataCheck = MutableLiveData<String>()
    private var _liveDataInsert = MutableLiveData<OutgoingVo>()
    private var _liveDataUpdate = MutableLiveData<OutgoingVo>()
    private var _liveDataDelete = MutableLiveData<OutgoingVo>()
    private var _liveDataOutgoingsByMonth = MutableLiveData<List<OutgoingVo>>()

    var liveDataCheck : LiveData<String> = _liveDataCheck
    var liveDataInsert: LiveData<OutgoingVo> = _liveDataInsert
    var liveDataDelete: LiveData<OutgoingVo> = _liveDataDelete
    var liveDataUpdate: LiveData<OutgoingVo> = _liveDataUpdate


    fun getOutgoingsByMonth(month:String) : LiveData<List<OutgoingVo>>{
        _liveDataOutgoingsByMonth.value = outgoingUseCase.getOutgoingsByMonth(month)
        return _liveDataOutgoingsByMonth
    }

    fun insert(data:OutgoingVo) : Int{
        when {
            data.info.isEmpty() -> {
                _liveDataCheck.value = Messages.INFO
                return -1
            }
            data.cost < 0 -> {
                _liveDataCheck.value = Messages.COST
                return -1
            }
            outgoingUseCase.isHasExpense(data) -> {
                outgoingUseCase.update(data)
                _liveDataUpdate.value = data
                _liveDataCheck.value = Messages.UPDATED
            }
            data.type < 0 ->{
                _liveDataCheck.value = Messages.UNCHECK
                return -1
            }
            else -> {
                data.id = outgoingUseCase.insert(data)
                _liveDataInsert.value = data
                _liveDataCheck.value = Messages.INSERTED
            }
        }
        return 1
    }

    fun delete(data: OutgoingVo){
        outgoingUseCase.delete(data)
        _liveDataDelete.value = data
        _liveDataCheck.value = Messages.DELETED
    }
}