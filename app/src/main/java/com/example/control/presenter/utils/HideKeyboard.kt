package com.example.control.presenter.utils

import android.app.Activity
import android.content.Context
import android.graphics.Rect
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import com.example.control.R


internal object KeyboardUtils {

    fun hideKeyboard(activity: Activity, editViewCost: View,editViewInfo: View) {
        val view: View? = activity.currentFocus
        val imm: InputMethodManager = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view?.windowToken, 0)
        (editViewCost as EditText).setText("")
        (editViewInfo as EditText).setText("")
    }

    fun showKeyboard(activity: Activity) {
        val inputMethodManager: InputMethodManager =
            activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
    }

    fun addKeyboardVisibilityListener(
        rootLayout: View,
        onKeyboardVisibilityListener: OnKeyboardVisibilityListener
    ) {
        rootLayout.viewTreeObserver.addOnGlobalLayoutListener {
            val r = Rect()
            rootLayout.getWindowVisibleDisplayFrame(r)
            val screenHeight: Int = rootLayout.rootView.height
            val keypadHeight: Int = screenHeight - r.bottom
            val isVisible = keypadHeight > screenHeight * 0.15
            onKeyboardVisibilityListener.onVisibilityChange(isVisible)
        }
    }

    interface OnKeyboardVisibilityListener {
        fun onVisibilityChange(isVisible: Boolean)
    }
}