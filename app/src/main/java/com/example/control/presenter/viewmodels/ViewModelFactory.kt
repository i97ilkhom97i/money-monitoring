package com.example.control.presenter.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.control.data.vo.OutgoingVo
import com.example.control.domain.usecases.OutgoingUseCase
import java.lang.IllegalArgumentException

class ViewModelFactory(private val useCase: OutgoingUseCase) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(OutgoingViewModel::class.java)){
            return OutgoingViewModel(useCase) as T
        }
        throw IllegalArgumentException("Unknown viewModel class !")
    }
}