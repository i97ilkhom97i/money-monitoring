package com.example.control.presenter.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.control.data.vo.OutgoingVo
import com.example.control.domain.usecases.OutgoingUseCase
import javax.inject.Inject

class OutgoingViewModelFullData @Inject constructor(private val useCase: OutgoingUseCase) : ViewModel() {
    private var _liveDataOutgoings = MutableLiveData<List<OutgoingVo>>()
    fun getOutgoings() : LiveData<List<OutgoingVo>> {
        _liveDataOutgoings.value = useCase.getOutgoings()
        return _liveDataOutgoings
    }
}