package com.example.control.presenter.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.control.R
import com.example.control.data.vo.OutgoingVo
import com.example.control.databinding.FirstItemBinding
import com.example.control.presenter.utils.Type
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class OutgoingAdapter () : RecyclerView.Adapter<OutgoingAdapter.VH>() {
    private val expenses = ArrayList<OutgoingVo>()
    private var updateListener : ((OutgoingVo) ->Unit) ?= null
    private var deleteListener : ((OutgoingVo) ->Unit) ?= null
    private var clickListener : ((OutgoingVo) ->Unit) ?= null
    private lateinit var binding : FirstItemBinding

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = VH (LayoutInflater.from(parent.context).inflate(
        R.layout.first_item,parent,false))

    override fun onBindViewHolder(holder: VH, position: Int) = holder.bind()

    override fun getItemCount(): Int = expenses.size

    inner class VH(view: View) : RecyclerView.ViewHolder(view){
        fun bind(){
            itemView.apply {
                binding = FirstItemBinding.bind(this)
                val data = expenses[adapterPosition]
                var date = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(data.date)
                binding.itemCost.text = data.cost.toString()
                binding.itemNumber.text = "${adapterPosition+1}. "
                binding.itemTitle.text = data.info
                binding.itemDate.text = SimpleDateFormat("HH:mm", Locale.getDefault()).format(date)
                if (data.type == Type.INCOME){
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                        binding.itemCard.setCardBackgroundColor(resources.getColor(R.color.income,null))
                    }
                }
                else{
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                        binding.itemCard.setCardBackgroundColor(resources.getColor(R.color.expense,null))
                    }
                }
                binding.itemDelete.setOnClickListener { deleteListener?.invoke(data) }
                binding.itemUpdate.setOnClickListener { updateListener?.invoke(data) }
                binding.itemCard.setOnClickListener { clickListener?.invoke(data) }
            }
        }
    }

    fun submitList(ls:List<OutgoingVo>){
        expenses.clear()
        expenses.addAll(ls)
        notifyItemRangeChanged(0,ls.size-1)
    }

    fun insert(outgoingVo: OutgoingVo){
        expenses.add(outgoingVo)
        notifyItemInserted(expenses.lastIndex)
    }


    fun delete(outgoingVo: OutgoingVo){
        var index = -1
        for (i in 0 until expenses.size){
            if (outgoingVo.id == expenses[i].id){
                index = i
            }
        }
        if (index >= 0){
            expenses.removeAt(index)
            notifyItemRemoved(index)
            notifyItemRangeChanged(index,expenses.size-index)
        }
    }

    fun update(outgoingVo: OutgoingVo){
        var index = -1
        for (i in 0 until expenses.size){
            if (outgoingVo.id == expenses[i].id){
                index = i
            }
        }
        if (index >= 0){
            expenses[index].cost = outgoingVo.cost
            expenses[index].info = outgoingVo.info
            expenses[index].type = outgoingVo.type
            notifyItemChanged(index)
        }
    }

    fun setUpdateListener(f : ((OutgoingVo) -> Unit)){
        updateListener = f
    }

    fun setDeleteListener(f : ((OutgoingVo) -> Unit)){
        deleteListener = f
    }

    fun setClickListener(f: ((OutgoingVo) -> Unit)){
        clickListener = f
    }
}