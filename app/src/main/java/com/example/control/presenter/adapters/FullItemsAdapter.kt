package com.example.control.presenter.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.control.R
import com.example.control.data.vo.OutgoingVo
import com.example.control.databinding.SecondItemBinding
import com.example.control.presenter.utils.Type
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class FullItemsAdapter () : RecyclerView.Adapter<FullItemsAdapter.VH>(){
    private val expenses = ArrayList<OutgoingVo>()
    private var listener: ((OutgoingVo) -> Unit) ?= null
    private lateinit var binding : SecondItemBinding
    inner class VH(view: View) : RecyclerView.ViewHolder(view){

        fun bind(){
            itemView.apply {
                binding = SecondItemBinding.bind(this)
                val data = expenses[adapterPosition]
                var date = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.forLanguageTag("Uzb")).parse(data.date)
                binding.itemCost.text = data.cost.toString()
                binding.itemNumber.text = "${adapterPosition+1}. "
                binding.itemTitle.text = data.info
                binding.itemDate.text = SimpleDateFormat("HH:mm", Locale.forLanguageTag("Uzb")).format(date)
                if (data.type == Type.INCOME){
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                        binding.itemCard.setCardBackgroundColor(resources.getColor(R.color.income,null))
                    }
                }
                else{
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                        binding.itemCard.setCardBackgroundColor(resources.getColor(R.color.expense,null))
                    }
                }
                binding.itemCard.setOnClickListener {  listener?.invoke(data)}
            }
        }
    }

    fun submitList(ls:List<OutgoingVo>){
        expenses.clear()
        expenses.addAll(ls)
        notifyItemRangeInserted(0,ls.size-1)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = VH (LayoutInflater.from(parent.context).inflate(R.layout.second_item,parent,false))

    override fun onBindViewHolder(holder: VH, position: Int) = holder.bind()

    override fun getItemCount(): Int = expenses.size

    fun clickInfo(f : (OutgoingVo) -> Unit){
        listener = f
    }

}