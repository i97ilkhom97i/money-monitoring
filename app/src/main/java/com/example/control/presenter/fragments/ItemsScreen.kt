package com.example.control.presenter.fragments

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.control.R
import com.example.control.data.vo.OutgoingVo
import com.example.control.databinding.ItemScreenBinding
import com.example.control.presenter.adapters.FullItemsAdapter
import com.example.control.presenter.utils.Messages
import com.example.control.presenter.viewmodels.OutgoingViewModelFullData
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class ItemsScreen : DaggerFragment(R.layout.item_screen) {

    @Inject
    lateinit var viewModel: OutgoingViewModelFullData

    @Inject
    lateinit var adapter: FullItemsAdapter

    private lateinit var binding: ItemScreenBinding
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding = ItemScreenBinding.bind(view)
        super.onViewCreated(view, savedInstanceState)
        binding.recycle.adapter = adapter
        binding.recycle.layoutManager = LinearLayoutManager(requireContext())
        viewModel.getOutgoings().observe(viewLifecycleOwner,{
            adapter.submitList(it)
        })
        adapter.clickInfo { clickInfo(it) }
    }

    private fun clickInfo(it: OutgoingVo) {
        AlertDialog.Builder(requireContext()).setTitle(Messages.TITLE)
                .setMessage("Narxi: ${it.cost} \n\nVaqti: ${it.date} \n\nIzoh: ${it.info}")
                .setNegativeButton("Yopish", null).create().show()
    }
}