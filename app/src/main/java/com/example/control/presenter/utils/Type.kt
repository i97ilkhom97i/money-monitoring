package com.example.control.presenter.utils

object Type {
    const val INCOME = 5
    const val EXPENSE = 6
}