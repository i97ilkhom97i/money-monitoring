package com.example.control.presenter.utils

import com.example.control.data.vo.OutgoingVo
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class ListHelper() {
    private val expenses = ArrayList<OutgoingVo>()

    fun submitList(ls: List<OutgoingVo>) {
        expenses.clear()
        expenses.addAll(ls)
    }

    fun insert(outgoingVo: OutgoingVo): ArrayList<OutgoingVo> {
        expenses.add(outgoingVo)
        return expenses
    }

    fun getFullCost(): String {
        var month = "Currently month"
        var expense: Long = 0
        var income: Long = 0
        for (i in 0 until expenses.size){
            val outgoing = expenses[i]
            if (outgoing.type == Type.EXPENSE){
                expense += outgoing.cost
            }
            else{
                income += outgoing.cost
            }
            val date = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).parse(outgoing.date)

            when(SimpleDateFormat("MM", Locale.getDefault()).format(date)){
                "01" -> {month = "Yanvar"}
                "02" -> {month = "Fevral"}
                "03" -> {month = "Mart"}
                "04" -> {month = "Aprel"}
                "05" -> {month = "May"}
                "06" -> {month = "Iyun"}
                "07" -> {month = "Iyul"}
                "08" -> {month = "Avgust"}
                "09" -> {month = "Sentyabr"}
                "10" -> {month = "Oktyabr"}
                "11" -> {month = "Noyabr"}
                "12" -> {month = "Dekabr"}
            }
        }
        return "$month: Kirim-$income   chiqim-$expense"
    }


    fun update(outgoingVo: OutgoingVo): ArrayList<OutgoingVo> {
        var index = -1
        for (i in 0 until expenses.size) {
            if (outgoingVo.id == expenses[i].id)
                index = i
        }

        if (index >= 0) {
            expenses.removeAt(index)
            expenses.add(index, outgoingVo)
        }
        return expenses
    }

    fun delete(outgoingVo: OutgoingVo): ArrayList<OutgoingVo> {
        var index = -1
        for (i in 0 until expenses.size) {
            if (outgoingVo.id == expenses[i].id)
                index = i
        }
        if (index >= 0) {
            expenses.removeAt(index)
        }
        return expenses
    }
}