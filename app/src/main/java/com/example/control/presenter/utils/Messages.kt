package com.example.control.presenter.utils

object Messages {
    const val INFO = "Qisqacha izox kiriting !"
    const val COST = "Miqdorini kiriting !"
    const val UPDATED = "Ma'lumot o'zgartirildi !"
    const val INSERTED = "Ma'lumot qo'shildi !"
    const val DELETED = "Ma'lumot o'chirildi !"
    const val UNCHECK = "Chiqim yoki kirim ekanligini belgilang"
    const val TITLE = "Ma'lumot"
}