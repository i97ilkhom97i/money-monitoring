package com.example.control.presenter.utils

import android.graphics.Color
import android.graphics.Typeface
import com.example.control.R
import com.example.control.data.vo.OutgoingVo
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet

class LoadChart() {
    private val ls = ArrayList<OutgoingVo>()
    fun submitList(list: ArrayList<OutgoingVo>) {
        ls.addAll(list)
    }

    fun loadBarChart(ls: ArrayList<OutgoingVo>): LineData {
        val entriesExpense = ArrayList<Entry>()
        val entriesIncome = ArrayList<Entry>()
        val lsExpense = ArrayList<OutgoingVo>()
        val lsIncome = ArrayList<OutgoingVo>()

        for (i in 0 until ls.size) {
            if (ls[i].type == Type.EXPENSE) {
                lsExpense.add(ls[i])
            } else {
                lsIncome.add(ls[i])
            }
        }

        entriesExpense.add(Entry(0f,0f))
        entriesIncome.add(Entry(0f,0f))

        for (i in 1 until lsExpense.size) {
            entriesExpense.add(Entry(i.toFloat(), lsExpense[i].cost.toFloat()))
        }

        for (i in 1 until lsIncome.size) {
            entriesIncome.add(Entry(i.toFloat(), lsIncome[i].cost.toFloat()))
        }

        val dataSets = ArrayList<ILineDataSet>()

        val barDataSet = LineDataSet(entriesExpense, "Chiqim")
        barDataSet.color = Color.parseColor("#F40404")
        barDataSet.mode = LineDataSet.Mode.HORIZONTAL_BEZIER

        val barDataSet2 = LineDataSet(entriesIncome, "Kirim")
        barDataSet2.color = Color.parseColor("#388E3C")
        barDataSet2.mode = LineDataSet.Mode.HORIZONTAL_BEZIER

        dataSets.add(barDataSet)
        dataSets.add(barDataSet2)

        return LineData(dataSets)
    }
}