package com.example.control.presenter.fragments

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.CheckBox
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.content.FileProvider
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.control.presenter.adapters.OutgoingAdapter
import com.example.control.R
import com.example.control.data.vo.OutgoingVo
import com.example.control.databinding.MainScreenBinding
import com.example.control.presenter.utils.*
import com.example.control.presenter.utils.KeyboardUtils
import com.example.control.presenter.viewmodels.OutgoingViewModel
import dagger.android.support.DaggerFragment
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class MainScreen : DaggerFragment(R.layout.main_screen) {
    private var outgoingId: Long = 0
    private lateinit var binding: MainScreenBinding

    @Inject
    lateinit var chart: LoadChart

    @Inject
    lateinit var checkList: ListHelper

    @Inject
    lateinit var adapter: OutgoingAdapter

    @Inject
    lateinit var viewModel: OutgoingViewModel

    private val checkObserver = Observer<String> { showToast(it) }
    private val insertObserver = Observer<OutgoingVo> { insert(it) }
    private val updateObserver = Observer<OutgoingVo> { update(it) }
    private val deleteObserver = Observer<OutgoingVo> { delete(it) }
    private val submitListObserver = Observer<List<OutgoingVo>> { submitList(it) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding = MainScreenBinding.bind(view)
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        binding.recycle.adapter = adapter
        binding.recycle.layoutManager = LinearLayoutManager(context)

        viewModel.liveDataCheck.observe(viewLifecycleOwner, checkObserver)
        viewModel.liveDataDelete.observe(viewLifecycleOwner, deleteObserver)
        viewModel.liveDataInsert.observe(viewLifecycleOwner, insertObserver)
        viewModel.liveDataUpdate.observe(viewLifecycleOwner, updateObserver)
        viewModel.getOutgoingsByMonth("${SimpleDateFormat("yyyy-MM", Locale.getDefault()).format(Calendar.getInstance().time)}%").observe(viewLifecycleOwner,submitListObserver)

        binding.addButton.setOnClickListener { clickAdd() }
        binding.checkboxIncome.setOnClickListener { checkCheckBox(it as CheckBox) }
        binding.checkboxExpense.setOnClickListener { checkCheckBox(it as CheckBox) }
        binding.menuBottom.setOnNavigationItemSelectedListener { selectMenu(it.itemId) }

        adapter.setClickListener { clickInfo(it) }
        adapter.setUpdateListener { clickUpdate(it) }
        adapter.setDeleteListener { viewModel.delete(it) }
    }

    private fun getOutgoingId(): Long = outgoingId
    private fun getInfo(): String = binding.info.text.toString()
    private fun setTextCost() { binding.wholeExpense.text = checkList.getFullCost() }
    private fun showToast(message: String) { Toast.makeText(context, message, Toast.LENGTH_SHORT).show() }
    private fun getType(): Int = if (binding.checkboxExpense.isChecked) { Type.EXPENSE } else { Type.INCOME }
    private fun getCost(): Long = if (binding.cost.text.isEmpty()) { -1 } else { binding.cost.text.toString().toLong() }
    private fun getDate(): String = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(Calendar.getInstance().time)

    private fun checkCheckBox(view: CheckBox) {
        when (view) {
            binding.checkboxExpense -> binding.checkboxIncome.isChecked = false
            binding.checkboxIncome -> binding.checkboxExpense.isChecked = false
        }
    }

    private fun insert(it: OutgoingVo) {
        adapter.insert(it)
        loadBarChart(checkList.insert(it))
        setTextCost()
    }

    private fun update(it: OutgoingVo) {
        adapter.update(it)
        loadBarChart(checkList.update(it))
        setTextCost()
    }

    private fun delete(it: OutgoingVo) {
        adapter.delete(it)
        loadBarChart(checkList.delete(it))
        setTextCost()
    }

    private fun selectMenu(id: Int): Boolean {
        when (id) {
            R.id.history -> { parentFragmentManager.beginTransaction().replace(R.id.main_activity, ItemsScreen()).addToBackStack("MainScreen").commit() }
            R.id.share -> { sendApkFile(requireContext()) }
        }
        return true
    }

    private fun submitList(ls: List<OutgoingVo>) {
        adapter.submitList(ls)
        checkList.submitList(ls)
        chart.submitList(ArrayList(ls))
        loadBarChart(ArrayList(ls))
        setTextCost()
    }

    private fun clickUpdate(it: OutgoingVo) {
        KeyboardUtils.showKeyboard(requireActivity())
        binding.cost.setText(it.cost.toString())
        binding.info.setText(it.info)
        outgoingId = it.id
    }

    private fun loadBarChart(ls: ArrayList<OutgoingVo>) {
        binding.chart.data = chart.loadBarChart(ls)
        binding.chart.animateY(4000)
    }

    private fun clickInfo(it: OutgoingVo) {
        AlertDialog.Builder(requireContext()).setTitle(Messages.TITLE)
                .setMessage("Narxi: ${it.cost} \nVaqti: ${it.date} \nIzoh: ${it.info}")
                .setNegativeButton("Yopish", null).create().show()
    }

    private fun clickAdd() {
        val outgoing = OutgoingVo(getOutgoingId(), getCost(), getInfo(), getDate(), getType())
        if (viewModel.insert(outgoing)> 0){
            KeyboardUtils.hideKeyboard(requireActivity(), binding.cost, binding.info)
        }
    }

    private fun sendApkFile(context: Context) {
        try {
            val pm = context.packageManager
            val ai = pm.getApplicationInfo(context.packageName, 0)
            val srcFile = File(ai.publicSourceDir)
            val intent = Intent(Intent.ACTION_SEND)
            intent.type = "*/*"
            val uri: Uri = FileProvider.getUriForFile(requireContext(), context.packageName, srcFile)
            intent.putExtra(Intent.EXTRA_STREAM, uri)
            context.grantUriPermission(
                    context.packageManager.toString(),
                    uri,
                    Intent.FLAG_GRANT_WRITE_URI_PERMISSION or Intent.FLAG_GRANT_READ_URI_PERMISSION
            )
            context.startActivity(intent)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}