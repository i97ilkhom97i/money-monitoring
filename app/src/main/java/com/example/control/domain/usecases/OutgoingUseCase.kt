package com.example.control.domain.usecases

import com.example.control.data.vo.OutgoingVo
import com.example.control.domain.repositories.OutgoingRepository
import javax.inject.Inject

class OutgoingUseCase @Inject constructor(private val repository: OutgoingRepository) {

    fun getOutgoings() : List<OutgoingVo> = repository.getAll()

    fun getOutgoingsByMonth(month:String) : List<OutgoingVo> = repository.getByMonth(month)

    fun insert(data:OutgoingVo) : Long = repository.insert(data)

    fun update(data: OutgoingVo) : Int = repository.update(data)

    fun delete(data: OutgoingVo) : Int = repository.delete(data)

    fun isHasExpense(data: OutgoingVo) : Boolean = repository.isHasData(data)
}
