package com.example.control.domain.repositories

import com.example.control.data.vo.OutgoingVo

interface OutgoingRepository {
    fun insert(data:OutgoingVo): Long
    fun update(data:OutgoingVo) : Int
    fun delete(data:OutgoingVo) : Int
    fun isHasData(data: OutgoingVo) : Boolean
    fun getAll() : List<OutgoingVo>
    fun getByMonth(month:String) : List<OutgoingVo>
}