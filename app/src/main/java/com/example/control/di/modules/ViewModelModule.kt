package com.example.control.di.modules

import androidx.lifecycle.ViewModel
import com.example.control.domain.usecases.OutgoingUseCase
import com.example.control.presenter.utils.ViewModelKey
import com.example.control.presenter.viewmodels.OutgoingViewModel
import com.example.control.presenter.viewmodels.OutgoingViewModelFullData
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap

@Module
class ViewModelModule {

    @Provides
    @IntoMap
    @ViewModelKey(OutgoingViewModel::class)
    fun getOutgoingViewModel(useCase: OutgoingUseCase) : ViewModel = OutgoingViewModel(useCase)

    @Provides
    @IntoMap
    @ViewModelKey(OutgoingViewModelFullData::class)
    fun getOutgoingViewModelFullData(useCase: OutgoingUseCase): ViewModel = OutgoingViewModelFullData(useCase)
}