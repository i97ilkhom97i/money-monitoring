package com.example.control.di.components

import android.content.Context
import com.example.control.app.App
import com.example.control.di.modules.BindRepositoryModule
import com.example.control.di.modules.ActFragModule
import com.example.control.di.modules.RepositoryModule
import com.example.control.di.modules.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton


@Component(
        modules = [
            AndroidInjectionModule::class,
            BindRepositoryModule::class,
            RepositoryModule::class,
            ActFragModule::class,
            ViewModelModule::class
        ]
)
@Singleton
interface AppComponent : AndroidInjector<App> {
    @Component.Factory
    interface Builder {
        fun create(@BindsInstance context: Context): AppComponent
    }
}