package com.example.control.di.modules

import com.example.control.data.repositories.OutgoingRepositoryImpl
import com.example.control.domain.repositories.OutgoingRepository
import dagger.Binds
import dagger.Module

@Module
interface BindRepositoryModule {

    @Binds
    fun getOutgoingRepository(impl: OutgoingRepositoryImpl) : OutgoingRepository
}