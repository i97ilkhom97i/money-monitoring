package com.example.control.di.modules

import com.example.control.presenter.activities.MainActivity
import com.example.control.presenter.fragments.ItemsScreen
import com.example.control.presenter.fragments.MainScreen
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface ActFragModule {

    @ContributesAndroidInjector
    fun mainActivity() : MainActivity

    @ContributesAndroidInjector
    fun mainScreen() : MainScreen

    @ContributesAndroidInjector
    fun itemScreen() : ItemsScreen
}