package com.example.control.di.modules

import android.content.Context
import com.example.control.data.mappers.Mapper
import com.example.control.data.repositories.OutgoingRepositoryImpl
import com.example.control.data.room.daos.OutgoingDao
import com.example.control.data.room.databases.AppDatabase
import com.example.control.data.vo.OutgoingVo
import com.example.control.presenter.adapters.FullItemsAdapter
import com.example.control.presenter.adapters.OutgoingAdapter
import com.example.control.presenter.utils.ListHelper
import com.example.control.presenter.utils.LoadChart
import dagger.Module
import dagger.Provides
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Singleton
import kotlin.collections.ArrayList

@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun getAppDatabase(context: Context) : AppDatabase = AppDatabase.getDatabase(context)

    @Provides
    @Singleton
    fun getOutgoingDao(database: AppDatabase) : OutgoingDao = database.getOutgoingDao()

    @Provides
    @Singleton
    fun getOutgoingImpl(dao: OutgoingDao) : OutgoingRepositoryImpl = OutgoingRepositoryImpl(dao)

    @Provides
    @Singleton
    fun getAllOutgoings(dao: OutgoingDao) : ArrayList<OutgoingVo> = ArrayList(dao.getOutgoings().map { Mapper.fromEntityToVo(it) })

    @Provides
    @Singleton
    fun getMainScreenAdapter() : OutgoingAdapter = OutgoingAdapter()

    @Provides
    @Singleton
    fun getItemsScreenAdapter() : FullItemsAdapter = FullItemsAdapter()

    @Provides
    @Singleton
    fun getCheckList() : ListHelper = ListHelper()

    @Provides
    @Singleton
    fun getChart() : LoadChart = LoadChart()
}